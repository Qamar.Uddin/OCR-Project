try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
import re
import string
from inspect import getmembers
from pprint import pprint
from pdf2image import convert_from_path

def ocr_func(filename): 
    """
    This function will handle the OCR func processing of images.
    """
    filename = "verok.jpeg"        #input file( tax card in image format or in pdf format) 
    text = pytesseract.image_to_string(Image.open(filename))  # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
    text_array = str.split(text)     #spliting the text into array of string

    ssn_re = re.compile("[0-9]{6}[A\\-+]{1}[0-9]{3}[A-Z0-9]{1}")    # reguler expression for finding social security number
    ssn = list(filter(ssn_re.match, text_array))
    ssnToStr = ' '.join(str(elem) for elem in ssn)
    ssn_index = text_array.index(ssnToStr)
    # end of code block for processing and finding of social security number

    # using backward indexing to find the name of customer by while loop
    name = [] 
    while(text_array[ssn_index - 1][0] in string.ascii_uppercase):
        name.append((text_array[ssn_index - 1]).replace(",", ''))
        ssn_index -= 1
    
    name.reverse()
    name = " ".join(name)
    # end of name code


    # start of the code for finding the range of the date
    date_re = re.compile("^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*[-]\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2}).\s*$")     # reguler expression for finding date range 
    date_range = list(filter(date_re.match, text_array))       #matching RE of date range from the list of string in text arry
    if(len(date_range) == 1):
        date_range = date_range[0]
    elif(len(date_range) > 1):
        date_range = date_range[1]

    index_of_dash = date_range.index("-")               # finding the index of dash which is in between two date e.g(01.03.2021-31.12.2021)
    starting_date = date_range[0:index_of_dash]         # start of the date on tax card
    ending_date_index = date_range[index_of_dash + 1:]
    last_index_period = starting_date.rfind(".")
    ending_date = ending_date_index[0:-1]               # start of the date on tax card
    #end of the date range and start & end date


    year_list_data = text_array[:ssn_index]    #start of the block 
    #from this list of string we find the four digit year using nested if and isnumeric fun 
    tax_year = ''
    for item in year_list_data:
        if(len(item) == 4):
            if(item.isnumeric()):
                tax_year = item                     
    #end of block

    #block of code for finding vero tax number 
    vero_number = ''
    for num in text_array:
        if(len(num) ==12):
            if(num.isnumeric()):
                vero_number = num
    #end of block

    #block of code for finding Withholding Tax Rate, Income Limit and Additional Tax percent
    date_range_index = text_array.index(date_range)
    tax_rate_data = text_array[ssn_index:date_range_index]    #creating array of string which contain indexes from social security number to date range index(all three required elements index inclued here) 
    tax_rate_location = ''                                    # find locations or idex of taxrate
    for tax_rate_data_item in tax_rate_data:
        if("," in tax_rate_data_item):
            tax_rate_temp = tax_rate_data_item.split(",")
            if(len(tax_rate_temp) == 2):
                if(tax_rate_temp[0].isnumeric() and tax_rate_temp[1].isnumeric()):
                    tax_rate_location = text_array.index(tax_rate_data_item)
    
    tax_rate_data_list = [text_array[tax_rate_location - 3], text_array[tax_rate_location - 2], text_array[tax_rate_location]]
     
    tax_rate = tax_rate_data_list[0].replace(",", ".")
    additional_tax_percent = tax_rate_data_list[2].replace(",", ".")
    tax_rate_index = text_array.index(tax_rate_data_list[0])
    additional_tax_percent_idex = text_array.index(tax_rate_data_list[2])
    income_limit = text_array[tax_rate_index + 1:additional_tax_percent_idex]
    income_limit = ''.join([str(elem) for elem in income_limit])
    income_limit = income_limit.replace(",", ".")

    print("Taxcard year =" , tax_year)                              #print year of the tax card
    print("Social Security Number =" ,ssnToStr)                     #print social security number 
    print("Name = " , name)                                         #print social name 
    print("Withholding Tax Rate = " , tax_rate)                     #print withholding tax rate 
    print("Income Limit = " , income_limit)                         #print income limit 
    print("Additional Tax Percent = " , additional_tax_percent)     #print additional tax percent
    print("Starting Date = " , starting_date)                       #starting date  
    print("Ending Date = " , ending_date)                           #print ending date
    print('Vero Tax Number =' ,vero_number)                         #print veronumber 

text = ''
ocr_func(text)               # calling the function to retun text from image 